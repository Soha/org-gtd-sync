#!/bin/bash

ORG_DIR="$HOME/gtd"
ORG_REMOTE_DIR="org"
REMOTE="org-drive"

case "$1" in
    start)
        rclone sync "$REMOTE:$ORG_REMOTE_DIR" "$ORG_DIR"
        ;;
    once)
        rclone sync "$ORG_DIR" "$REMOTE:$ORG_REMOTE_DIR"
        ;;
    test)
        find "$ORG_DIR"
        echo "entr -r rclone sync ${ORG_DIR} ${REMOTE}:${ORG_REMOTE_DIR}"
        ;;
    *)
        find "$ORG_DIR" | entr -r rclone sync "$ORG_DIR" "$REMOTE:$ORG_REMOTE_DIR"
        ;;
esac
